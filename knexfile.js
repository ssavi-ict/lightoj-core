const {db} = require('config')
/**
 * Database configuration.
 */
module.exports = {
  client: 'mysql2',
  connection: {
    port: 3306,
    host: db.host,
    user: db.username,
    password: db.password,
    database: db.database,
    charset: 'utf8',
    timezone: 'UTC'
  },
  migrations: {
    tableName: 'migrations',
    directory: './server/migrations'
  },
  seeds: {
    directory: './server/seeds'
  }
}
