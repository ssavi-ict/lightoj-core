import knex from 'knex'
import knexConfig from '../../knexfile'

jest.useFakeTimers()
jest.setTimeout(30000)

let db

beforeAll(async () => {
  db = knex(knexConfig)
  await db.migrate.latest()
  // await db.seed.run()
})

afterAll(async () => db.migrate.rollback())
