// import {assert} from 'chai'
import request from 'supertest'
import server from '../../server'

/**
 * Testing post user endpoint
 */

describe('POST /api/auth/register', function () {
  it('successful registration', async () => {
    let payload = {
      email: 'testuser@test.com',
      fullname: 'Test User',
      handle: 'testUser',
      password: 'test123'
    }
    let response = {
      'success': true,
      'msg': 'Successfully created account. Please verify the account'
    }

    await request(server)
      .post('/api/auth/register')
      .send(payload)
      .expect(200)
      .then((res) => {
        expect(res.body).toMatchObject(response)
      })
  })
  test('instantiate', () => {
    let a = 200
    expect(a).toBe(200)
  })
})
