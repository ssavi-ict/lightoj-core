import cookies from 'js-cookie'

const tokenCookieName = 'token'

export const state = () => ({
  loggedIn: false,
  userData: {}
})

export const mutations = {
  INIT_LOGIN (state, user) {
    state.loggedIn = true
    state.userData = user
  },
  LOGGED_IN (state, user) {
    state.loggedIn = true
    state.userData = user.userData
  },
  LOGGED_OUT (state) {
    state.loggedIn = false
    state.userData = {}
  }
}

export const actions = {
  logout ({commit}) {
    commit('LOGGED_OUT')
    cookies.remove(tokenCookieName)
  },
  async login ({commit}, data) {
    /*
      data should contain two fields:
      handleOrEmail, password
    */
    let returnData = {
      'success': true,
      'errors': []
    }
    try {
      const resp = await this.$axios.$post('auth/login', {
        handleOrEmail: data.handleOrEmail,
        password: data.password
      })
      commit('LOGGED_IN', resp)
      cookies.set(tokenCookieName, resp.token)
    } catch (e) {
      returnData.success = false
      if (e.response.status === 422) {
        /* its a validation error */
        returnData.errors = e.response.data.errors
      } else {
        returnData.errors = ['Something weird! Please try again.']
      }
    }
    return returnData
  },
  async register ({commit}, data) {
    /*
      data should contain four fields:
      fullname, handle, email, password
    */
    let returnData = {
      'success': true,
      'errors': []
    }
    try {
      await this.$axios.$post('auth/register', data)
      this.$router.push('/auth/login')
    } catch (e) {
      console.log(e)
      returnData.success = false
      if (e.response.status === 422) {
        /* its a validation error */
        returnData.errors = e.response.data.errors
      } else {
        returnData.errors = ['Something weird! Please try again.']
      }
    }
    return returnData
  }
}
