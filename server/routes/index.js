import { Router } from 'express'
import auth from './auth'

const router = Router()

// Add USERS Routes
router.use('/auth', auth)

function errorHandler (err, req, res, next) {
  res.status(500)
  res.json({ error: err })
}

router.use(errorHandler)

export default router
