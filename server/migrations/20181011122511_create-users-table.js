
exports.up = function (knex, Promise) {
  return knex.schema.createTable('users', function (t) {
    t.bigIncrements('userId').unsigned().primary()
    t.timestamps()
    t.string('userNameStr').notNullable()
    t.string('userEmailStr').unique().notNullable()
    t.string('userHandleStr', 30).unique().notNullable()
    t.string('userPassStr').notNullable()
  })
}

exports.down = function (knex, Promise) {
  return knex.schema.dropTable('users')
}

exports.down = function (knex, Promise) {
  return knex.schema.dropTableIfExists('users')
}
