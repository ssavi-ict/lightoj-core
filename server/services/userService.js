import bcrypt from 'bcrypt'

import User from '../models/user'

const getHashedPassword = async (password) => {
  const salt = await bcrypt.genSalt(10)
  const hash = await bcrypt.hash(password, salt)
  return hash
}

const matchPasswordWithHash = async (givenPassword, hashedPassword) => {
  const matches = await bcrypt.compare(givenPassword, hashedPassword)
  if (matches) return true
  return false
}

export async function createUser (userData) {
  const createData = {
    userNameStr: userData.fullname,
    userHandleStr: userData.handle,
    userEmailStr: userData.email,
    userPassStr: await getHashedPassword(userData.password)
  }
  console.log(createData)
  return new User(createData).save()
}

export async function checkEmailExist (email) {
  const userCount = await User.where('userEmailStr', email).count('userId')
  return (userCount > 0)
}

export async function checkHandleExist (handle) {
  const userCount = await User.where('userHandleStr', handle).count('userId')
  return (userCount > 0)
}

/**
 * Checks two things:
 *   1) if user is found for the handle/email and
 *   2) if the given password is same.
 */
export async function loginUserWithHandleOrEmail (handleOrEmail, password) {
  const user = await User
    .where(function () {
      this
        .where('userHandleStr', handleOrEmail)
        .orWhere('userEmailStr', handleOrEmail)
    })
    .fetch({'required': true})

  console.log('user found')
  console.log(user)

  if (!user) { return null } // No user found with the given handle
  const match = await matchPasswordWithHash(password, user.get('userPassStr'))
  return (match === true) ? user : null
}
